# Flask Market

A web application that uses Flask as backend and simple HTML in the frontend. It has registration and authentication functionality. You can see available items and buy/sell them. Items are stored in a local SQL database.

## Getting started

To run this project you need Python and pip3.

When you install python, clone the repository and in the project file path enter these commands:

<h3 style='text-align: center; vartical-align: middle;'>Terminal</h3>

```
pip3 install flask flask_sqlalchemy flask_bcrypt flask_login email_validator
```

```
python3 run.py
```

## Features

- Login and registration
- View items
- Buy items
- Sell items
