from market import app, db
from market.models import User, Item
# in terminal: export FLASK_APP=market.py
# in terminal: export FLASK_DEBUG=1
# then: flask run

# if __name__ == '__main__':
#   u1 = User(username='jsc', password_hash='123456', email_address='jsc@jsc.com')
#   with app.app_context():
#     db.session.add(u1)
#     db.session.commit()
#     print(User.query.all())
app.run(debug=True)

# if __name__ == '__main__':
#   item5 = Item(name="Laptop", price=500, barcode="123123123123", description="This is a budget Laptop")
#   with app.app_context():
#     db.session.add(item5)
#     db.session.commit()

#     print(Item.query.all()) # -> prints all items
#     for item in Item.query.all(): # -> prints all items and their description
#       print(item.name,
#       item.price,
#       item.description,
#       item.id,
#       item.barcode)
#     print("filter_by:\n")
#     print(Item.query.filter_by(price=500))
#     print("\nWith for loop:")
#     for item in Item.query.filter_by(price=500):
#       print(item.name)

# # inspect the database in a visual graphical user interface